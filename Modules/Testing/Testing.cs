﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Discord;
using Discord.Addons.EmojiTools;
using Discord.Addons.Interactive;
using Discord.Commands;
using Discord.WebSocket;
using Eshirixis.Common.Attributes;
using Eshirixis.Common.Enums;
using Eshirixis.Common.Extensions;
using Eshirixis.Common.Models;
using Eshirixis.Common.Types;
using ImageMagick;
using Eshirixis.Common.Utility;


namespace Eshirixis.Modules.Testing
{
    [Name("Testing"), Summary("contains testing commands."), MinPermissions(AccessLevel.BotOwner)]
    public class TestingModule : ModuleBase<SocketCommandContext>
    {
        private string _baseImageLoc = AppContext.BaseDirectory + "\\BaseImage.png";

        [Command("render")]
        [Remarks("Render image with given user.")]
        public async Task RenderImage(IUser user = null)
        {
            try
            {
                if (user == null) user = Context.User;
                using (var img = new MagickImage(user.GetAvatarUrl(ImageFormat.Png,128)))
                {
                    var basew = img.BaseWidth;
                    var baseh = img.BaseHeight;

                    img.AdaptiveResize(basew *2, baseh *2);
                    
                    img.Write(AppContext.BaseDirectory + "\\final.png");
                    await Context.Channel.SendFileAsync(AppContext.BaseDirectory + "\\final.png");
                }
            }
            catch(Exception ex)
            {
                var embed = NeoEmbeds.Minimal(ex.Message).AddField("inner", ex.InnerException?.Message ?? "nope");
                await Context.Channel.SendMessageAsync("", false, embed.Build());
            }            
        }

        [Command("render2")]
        [Remarks("Render image with given user.")]
        public async Task RenderImage2(int i = 0,IUser user = null)
        {
            try
            {
                if (user == null) user = Context.User;
                using (var img = new MagickImage(user.GetAvatarUrl(ImageFormat.Auto,256)))
                {
                    img.MedianFilter();
                    img.Write(AppContext.BaseDirectory + "\\final.png");
                    await Context.Channel.SendFileAsync(AppContext.BaseDirectory + "\\final.png");
                }
            }
            catch (Exception ex)
            {
                var embed = NeoEmbeds.Minimal(ex.Message).AddField("inner", ex.InnerException?.Message ?? "nope");
                await Context.Channel.SendMessageAsync("", false, embed.Build());
            }
        }
    }
}