﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Addons.Interactive;
using Discord.Commands;
using Discord.WebSocket;
using Eshirixis.Common.Extensions;
using Eshirixis.Common.Models;
using Eshirixis.Common.Types;

namespace Eshirixis.Modules.General
{
    [Name("Tag"), Summary("contains tag commands.")]
    public class TagModule : InteractiveBase<SocketCommandContext>
    {
        private static DiscordSocketClient C { get; set; }
        public InteractiveService S { get; set; }

        public TagModule(DiscordSocketClient c)
        {
            C = c;
        }

        [Group("tag"), Name("Tag Module")]
        public class TagModuleSub : InteractiveBase<SocketCommandContext>
        {
            [Command("create", RunMode = RunMode.Async), Alias("c")]
            [Remarks("Lets you create a tag [Guild Specific]")]
            [RequireContext(ContextType.Guild)]
            [Priority(1)]
            public async Task CreateTag([Summary("Trigger of the tag")][Remainder]string trigger)
            {
                using (var db = new NeoContext())
                {
                    if (db.Tags.Any(t =>
                        t.Guild == db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id) &&
                        t.Trigger == trigger))
                    {
                        var embed = NeoEmbeds.Minimal("Trigger already exists");
                        await ReplyAndDeleteAsync("", false, embed.Build(), TimeSpan.FromSeconds(5));
                        await Context.Message.DeleteAsync();
                        return;
                    }
                }

                var u = Context.User;
                var confirmation = NeoEmbeds.Minimal($"Creating a tag named `{trigger}`.").AddField("\u200B", "Please give a value (30 Seconds)");

                var cn = await ReplyAndDeleteAsync("", false, confirmation.Build(), TimeSpan.FromSeconds(30));

                var nm = await NextMessageAsync(timeout: TimeSpan.FromSeconds(30));
                if (nm != null)
                {
                    using (var db = new NeoContext())
                    {
                        var tagobj = new Tag
                        {
                            Creation = DateTime.Now,
                            Guild = db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id),
                            Owner = db.Users.FirstOrDefault(us => us.Id == u.Id),
                            Trigger = trigger,
                            Value = nm.Attachments.Any() ? nm.Attachments.FirstOrDefault()?.Url : nm.Content,
                            IfAttachment = nm.Attachments.Any(),
                            Uses = 0
                        };
                        db.Tags.Add(tagobj);
                        db.SaveChanges();
                        confirmation = NeoEmbeds.Minimal($"Created a tag named `{trigger}`.");
                        await ReplyAndDeleteAsync("", false, confirmation.Build(), TimeSpan.FromSeconds(10));
                        await cn.DeleteAsync();
                        await nm.DeleteAsync();
                        await Context.Message.DeleteAsync();
                    }
                }
                else
                {
                    var embed = NeoEmbeds.Minimal("Failed, no answer in 30 seconds.");
                    await ReplyAndDeleteAsync("", false, embed.Build(), TimeSpan.FromSeconds(5));
                    await Context.Message.DeleteAsync();
                    await nm.DeleteAsync();
                }
            }

            [Command("delete"), Alias("d")]
            [Remarks("Lets you delete a tag of yours [Guild Specific]")]
            [RequireContext(ContextType.Guild)]
            [Priority(1)]
            public async Task DeleteTag([Summary("Trigger of the tag")][Remainder]string trigger)
            {
                var owner = Configuration.Load().Owners[0];
                using (var db = new NeoContext())
                {
                    if (!db.Tags.Any(t =>
                        ((t.Guild == db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id)) || Context.User.Id == owner) &&
                        t.Trigger == trigger &&
                        (t.Owner == db.Users.FirstOrDefault(u => u.Id == Context.User.Id)) || Context.User.Id == owner))
                    {
                        var embed = NeoEmbeds.Minimal("Trigger does not exist or not yours.");
                        await ReplyAndDeleteAsync("", false, embed.Build(), TimeSpan.FromSeconds(5));
                        await Context.Message.DeleteAsync();
                        return;
                    }

                    var obj = db.Tags.FirstOrDefault(t => t.Trigger == trigger);
                    db.Tags.Remove(obj);
                    db.SaveChanges();
                    var embed2 = NeoEmbeds.Minimal("Deleted.");
                    await ReplyAndDeleteAsync("", false, embed2.Build(), TimeSpan.FromSeconds(5));
                    await Context.Message.DeleteAsync();
                }
            }


            [Command("stats"), Alias("s")]
            [Remarks("Shows the stats of the tag [Guild Specific]")]
            [RequireContext(ContextType.Guild)]
            [Priority(1)]
            public async Task StatTag([Summary("Trigger of the tag")][Remainder]string trigger)
            {
                using (var db = new NeoContext())
                {
                    if (db.Tags.Any(t =>
                        t.Guild == db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id) &&
                        t.Trigger == trigger))
                    {
                        var obj = db.Tags.FirstOrDefault(tx => tx.Trigger == trigger);
                        var owner = "placeholder";
                        var embed = NeoEmbeds.TagStats(owner, obj);
                        await ReplyAsync("", false, embed.Build());
                    }
                    else
                    {
                        var embed = NeoEmbeds.Minimal("Trigger does not exist");
                        await ReplyAndDeleteAsync("", false, embed.Build(), TimeSpan.FromSeconds(5));
                        await Context.Message.DeleteAsync();
                    }
                }
            }

            [Command("stats"), Alias("s")]
            [Remarks("Shows the stats of the tag module.")]
            [RequireContext(ContextType.Guild)]
            [RequireOwner]
            [Priority(1)]
            public async Task StatTag()
            {
                IQueryable<IGrouping<Guild, Tag>> mostguilds;
                using (var db = new NeoContext())
                {
                    mostguilds = db.Tags
                        .GroupBy(t => t.Guild)
                        .OrderByDescending(x => x)
                        .Take(5);

                    var builder = new EmbedBuilder
                    {
                        Color = new Color(255, 0, 0),
                        Description = "Tag Module Stats",
                        Author = new EmbedAuthorBuilder
                        {
                            Name = "NeoKapcho",
                            IconUrl = Context.Client.CurrentUser.GetAvatarUrl()
                        },
                        Footer = new EmbedFooterBuilder
                        {
                            Text = $"Command executed by {Context.User.Username}",
                            IconUrl = Context.User.GetAvatarUrl()
                        },
                        Timestamp = DateTime.Now
                    };

                    builder.AddField(x => {
                        x.Name = "Top guilds";
                        x.Value = new Func<string>(() =>
                        {
                            var z = "";
                            foreach (var g in mostguilds)
                            {
                                z += $"{Context.Client.GetGuild(g.Key.Id).Name}\n";
                            }
                            return z;
                        })();
                        x.IsInline = false;
                    });

                    builder.AddField(x => {
                        x.Name = "Total Tags";
                        x.Value = db.Tags.Count();
                        x.IsInline = true;
                    });

                    builder.AddField(x => {
                        x.Name = "Total Uses";
                        x.Value = db.Tags.Sum(z => z.Uses);
                        x.IsInline = true;
                    });

                    await ReplyAsync("", false, builder.Build());
                }
            }

            [Command("list"), Alias("l")]
            [Remarks("Shows the list of the tag [Guild Specific]")]
            [RequireContext(ContextType.Guild)]
            [RequireOwner]
            [Priority(1)]
            public async Task ListTag()
            {
                using (var db = new NeoContext())
                {
                    if (db.Tags.Any(t => t.Guild == db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id)))
                    {
                        var obj = db.Tags.ToList();
                        var currentIndex = 0;
                        const int limit = 10;
                        var count = obj.Count();
                        var ss = new List<string>();
                        do
                        {
                            var temp = "";
                            for (var i = currentIndex; i <= currentIndex + limit; i++)
                            {
                                if (i == count) break;
                                temp += (
                                    $"Trigger : {obj[i].Trigger}" +
                                    $"\nGuild ID : {obj[i].Guild.Id}" +
                                    $"\nOwner ID : {obj[i].Owner.Id}" +
                                    $"\nCreation : {obj[i].Creation.ToString("dd.MM.yyyy hh:mm", CultureInfo.InvariantCulture)}" +
                                    $"\n\n");
                            }
                            ss.Add(temp);
                            currentIndex += limit;
                        } while (currentIndex <= count);
                        await PagedReplyAsync(ss.ToArray());
                    }
                    else
                    {
                        var embed = NeoEmbeds.Minimal("Guilds has no tags");
                        await ReplyAndDeleteAsync("", false, embed.Build(), TimeSpan.FromSeconds(5));
                        await Context.Message.DeleteAsync();
                    }
                }
            }

        }
    }
}
