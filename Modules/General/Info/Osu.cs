﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using OsuSharp;
using OsuSharp.Misc;
using Eshirixis.Common.Attributes;
using Eshirixis.Common.Enums;
using Eshirixis.Common.Extensions;
using Eshirixis.Common.Models;
using Eshirixis.Services;

namespace Eshirixis.Modules.General
{    
    public partial class Info : ModuleBase<SocketCommandContext>
    {
        [Group("osu"), Name("Osu! Info")]
        public class InfoOsuSub : ModuleBase<SocketCommandContext>
        {
            [Command("")]
            [Remarks("Gets Osu! Standart mode info.")]
            [Priority(0)]
            public async Task GetosuStdInfo([Summary("Osu IGN")][Remainder] string name)
            {
                var embed = OsuHelper.GetUserProfileByNameAndModAsync(Context, name);
                await ReplyAsync("", embed: embed.Build());
            }

            [Command("bm")]
            [Remarks("test")]
            [Priority(1)]
            public async Task Getbmtest([Summary("Osu map id")][Remainder] ulong name)
            {
                var embed = OsuHelper.GetBeatmapDetailsFromId(Context, name);
                await ReplyAsync("", embed: embed.Build());
            }

            [Command("mania")]
            [Remarks("Gets Osu! Mania mode info.")]
            [Priority(1)]
            public async Task GetosumaniaInfo([Summary("Osu IGN")][Remainder] string name)
            {
                var embed = OsuHelper.GetUserProfileByNameAndModAsync(Context, name, GameMode.Mania);
                await ReplyAsync("", embed: embed.Build());
            }

            [Command("ctb")]
            [Remarks("Gets Osu! Ctb mode info.")]
            [Priority(1)]
            public async Task GetosuctbInfo([Summary("Osu IGN")][Remainder] string name)
            {
                var embed = OsuHelper.GetUserProfileByNameAndModAsync(Context, name, GameMode.Catch);
                await ReplyAsync("", embed: embed.Build());
            }

            [Command("taiko")]
            [Remarks("Gets Osu! Taiko mode info.")]
            [Priority(1)]
            public async Task GetosutaikoInfo([Summary("Osu IGN")][Remainder] string name)
            {
                var embed = OsuHelper.GetUserProfileByNameAndModAsync(Context, name, GameMode.Taiko);
                await ReplyAsync("", embed: embed.Build());
            }

            [Group("top5"), Name("Osu! top5 Info")]
            public class InfoTop5Sub : ModuleBase<SocketCommandContext>
            {
                [Command("")]
                [Remarks("Gets Osu! Standart mode info.")]
                [Priority(2)]
                public async Task GetosuStdInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserTop5ByNameAndModAsync(Context, name);
                    await ReplyAsync("", embed: embed.Build());
                }

                [Command("mania")]
                [Remarks("Gets Osu! Mania mode info.")]
                [Priority(3)]
                public async Task GetosumaniaInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserTop5ByNameAndModAsync(Context, name, GameMode.Mania);
                    await ReplyAsync("", embed: embed.Build());
                }

                [Command("ctb")]
                [Remarks("Gets Osu! Ctb mode info.")]
                [Priority(3)]
                public async Task GetosuctbInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserTop5ByNameAndModAsync(Context, name, GameMode.Catch);
                    await ReplyAsync("", embed: embed.Build());
                }

                [Command("taiko")]
                [Remarks("Gets Osu! Taiko mode info.")]
                [Priority(3)]
                public async Task GetosutaikoInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserTop5ByNameAndModAsync(Context, name, GameMode.Taiko);
                    await ReplyAsync("", embed: embed.Build());
                }
            }

            [Group("recent"), Name("Osu! recent Info")]
            public class InfoRecentSub : ModuleBase<SocketCommandContext>
            {
                [Command("")]
                [Remarks("Gets Osu! Standart mode info.")]
                [MinPermissions(AccessLevel.User)]
                [Priority(4)]
                public async Task GetosuStdInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserRecent5ByNameAndModAsync(Context, name);
                    await ReplyAsync("", embed: embed.Build());
                }

                [Command("mania")]
                [Remarks("Gets Osu! Mania mode info.")]
                [Priority(5)]
                public async Task GetosumaniaInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserRecent5ByNameAndModAsync(Context, name, GameMode.Mania);
                    await ReplyAsync("", embed: embed.Build());
                }

                [Command("ctb")]
                [Remarks("Gets Osu! Ctb mode info.")]
                [Priority(5)]
                public async Task GetosuctbInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserRecent5ByNameAndModAsync(Context, name, GameMode.Catch);
                    await ReplyAsync("", embed: embed.Build());
                }

                [Command("taiko")]
                [Remarks("Gets Osu! Taiko mode info.")]
                [Priority(5)]
                public async Task GetosutaikoInfo([Summary("Osu IGN")][Remainder] string name)
                {
                    var embed = OsuHelper.GetUserRecent5ByNameAndModAsync(Context, name, GameMode.Taiko);
                    await ReplyAsync("", embed: embed.Build());
                }
            }
        }

    }
}
