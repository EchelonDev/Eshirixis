﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using OsuSharp;
using OsuSharp.Misc;
using Eshirixis.Common.Extensions;
using Eshirixis.Common.Types;
using Eshirixis.Common.Utility;
using Eshirixis.Common.Models;

namespace Eshirixis.Services {
    public static class OsuHelper {
        public static string ApiKey = Configuration.Load ().OsuApiKey != "" ? Configuration.Load ().OsuApiKey : throw new Exception ("osu!key does not exists");

        public enum Mods {
            None = 0,
                NoFail = 1,
                Easy = 2,
                //NoVideo      = 4,
                Hidden = 8,
                HardRock = 16,
                SuddenDeath = 32,
                DoubleTime = 64,
                Relax = 128,
                HalfTime = 256,
                Nightcore = 512, // Only set along with DoubleTime. i.e: NC only gives 576
                Flashlight = 1024,
                Autoplay = 2048,
                SpunOut = 4096,
                Relax2 = 8192, // Autopilot?
                Perfect = 16384, // Only set along with SuddenDeath. i.e: PF only gives 16416  
                Key4 = 32768,
                Key5 = 65536,
                Key6 = 131072,
                Key7 = 262144,
                Key8 = 524288,
                KeyMod = Key4 | Key5 | Key6 | Key7 | Key8,
                FadeIn = 1048576,
                Random = 2097152,
                LastMod = 4194304,
                FreeModAllowed = NoFail | Easy | Hidden | HardRock | SuddenDeath | Flashlight | FadeIn | Relax | Relax2 | SpunOut | KeyMod,
                Key9 = 16777216,
                Key10 = 33554432,
                Key1 = 67108864,
                Key3 = 134217728,
                Key2 = 268435456
        }

        internal static string GetUserNameFromDb(SocketCommandContext context)
        {
            using(var db = new NeoContext()) {
                if(db.OsuNames.Any(x => x.User == db.Users.FirstOrDefault(u => u.Id == context.User.Id))) {
                    return db.OsuNames.FirstOrDefault(x => x.User == db.Users.FirstOrDefault(u => u.Id == context.User.Id)).Name;
                }
                else {
                    var embed = NeoEmbeds.Minimal("User has not set a username yet.").Build();
                    context.Channel.SendMessageAsync("",false,embed);
                    return null;
                }
            }
        }

        private static OsuApi api = OsuApi.Default(ApiKey);

        public static Mods[] GetArray (this Mods mods) {
            var modsList = new List<Mods> ();

            foreach (Mods value in Enum.GetValues (typeof (Mods)))
            if (mods.HasFlag (value))
                modsList.Add (value);

            return modsList.ToArray ();
        }

        public static string GetArray (this uint mods) {
            return Enum.Parse (typeof (Mods), mods.ToString ()).ToString ();
        }

        public static string ToFixed (this double num) {
            return String.Format ("{0:#,##0.##}", num);
        }

        public static string ToFixed(this ulong num)
        {
            return String.Format("{0:#,##0.##}", num);
        }

        public static string ToFixed (this float num) {
            return String.Format ("{0:#,##0.##}", num);
        }

        public static string ToFixed (this decimal num) {
            return String.Format ("{0:#,##0.##}", num);
        }

        public static string ToFixed (this uint num) {
            return String.Format ("{0:#,##0.##}", num);
        }

        public static string ToFixed (this int num) {
            return String.Format ("{0:#,##0.##}", num);
        }

        public static string ToFixed (this long num) {
            return String.Format ("{0:#,##0.##}", num);
        }

        public static string ModReplacer (string input) {
            input = input.Replace ("DoubleTime", "<:modDT:235857321463513091>");
            input = input.Replace ("Easy", "<:modEZ:235857322000384010>");
            input = input.Replace ("Flashlight", "<:modFL:235857322310762496>");
            input = input.Replace ("Hidden", "<:modHD:235857322541318144>");
            input = input.Replace ("HardRock", "<:modHR:235985782471524362>");
            input = input.Replace ("HalfTime", "<:modHT:235857322201710592>");
            input = input.Replace ("Nightcore", "<:modNC:235857323241897984>");
            input = input.Replace ("NoFail", "<:modNF:235857323170463746>");
            input = input.Replace ("Perfect", "<:modPF:235857322969268226>"); //SuddenDeath
            input = input.Replace ("SuddenDeath", "<:modSD:235857323170463747>");
            return input;
        }

        public static EmbedBuilder GetUserProfileByNameAndModAsync (SocketCommandContext c, string uname, GameMode mode = GameMode.Standard) {
            return Task.Run (async () => {
                try {
                    var u = await api.GetUserByNameAsync (uname, mode);

                    if (u == null) {
                        return NeoEmbeds.Minimal ($"Could not found user named `{uname}`");
                    }

                    var embed = new EmbedBuilder {
                        Color = new Color (0, 255, 0),
                        Author = new EmbedAuthorBuilder {
                        Name = uname,
                        IconUrl = $"http://a.ppy.sh/{u.Userid}",
                        Url = $"http://osu.ppy.sh/u/{u.Userid}"
                        },
                        Timestamp = DateTime.UtcNow,
                        Footer = new EmbedFooterBuilder {
                        Text = $"Command executed by {c.User.Username}",
                        IconUrl = c.User.GetAvatarUrl ()
                        }
                    };

                    var sb = new StringBuilder ();
                    var sb2 = new StringBuilder ();
                    sb.AppendLine ($"**Rank:** #{u.GlobalRank.ToFixed(), -40}");
                    sb.AppendLine ($"**Level:** {u.Level.ToFixed()}");
                    sb.AppendLine ($"**Total Score:** {u.TotalScore.ToFixed()}\n");

                    sb2.AppendLine ($"**Performance Points:** {u.Pp.ToFixed()} PP");
                    sb2.AppendLine ($"**Accuracy:** {u.Accuracy.ToFixed()}%");
                    sb2.AppendLine ($"**Ranked Score:** {u.RankedScore.ToFixed(), -40}\n");

                    sb.AppendLine ($"**Country:** :flag_{u.Country.ToLower()}:");
                    sb.AppendLine ($"**Country Rank:** #{u.RegionalRank.ToFixed(), -40}\n");

                    sb2.AppendLine ($"**Play Count:** {u.PlayCount.ToFixed()}");
                    if ((c.Guild.GetUser (c.Client.CurrentUser.Id)).Roles.Any (x => x.Permissions.UseExternalEmojis)) {
                        sb2.AppendLine ($"<:rankSS:235857321115385867> : {u.SsRank.ToFixed()} <:rankS:235857321010528256> : {u.SRank.ToFixed()} <:rankA:235857264278241284>: {u.ARank.ToFixed()}");
                        sb2.AppendLine ($"<:hit300:235857204090109952> : {u.Count300.ToFixed()} <:hit100:235857202923962368> : {u.Count100.ToFixed()} <:hit50:235857202257199104> : {u.Count50.ToFixed()}");
                    } else {
                        sb2.AppendLine ($"**SS:** {u.SsRank.ToFixed()} **S:** {u.SRank.ToFixed()} **A:** {u.ARank.ToFixed()}");
                        sb2.AppendLine ($"**300:** {u.Count300.ToFixed()} **100:** {u.Count100.ToFixed()} **50:** {u.Count50.ToFixed()}");
                    }

                    embed.AddField (x => {
                        x.Name = "Mode";
                        x.Value = mode.ToString ();
                        x.IsInline = false;
                    });

                    embed.AddField (x => {
                        x.Name = "Stats";
                        x.Value = sb.ToString ();
                        x.IsInline = true;
                    });

                    embed.AddField (x => {
                        x.Name = "\u200B";
                        x.Value = sb2.ToString ();
                        x.IsInline = true;
                    });

                    return embed;
                } catch (Exception ex) {
                    var embed = NeoEmbeds.Minimal (ex.Message).AddField ("inner", ex.InnerException?.Message ?? "nope");
                    await NeoConsole.NewLine (ex.StackTrace);
                    return embed;
                }
            }).Result;
        }

        public static EmbedBuilder GetUserTop5ByNameAndModAsync (SocketCommandContext c, string uname, GameMode mode = GameMode.Standard) {
            return Task.Run (async () => {
                try {
                    var u = await api.GetUserByNameAsync (uname, mode);
                    var scores = await api.GetUserBestByUsernameAsync (uname, mode, 5);

                    if (u == null || scores.Count <= 0) {
                        return NeoEmbeds.Minimal ($"Could not found scores by user `{uname}`");
                    }

                    var mc = await c.Channel.SendMessageAsync ("", false, NeoEmbeds.Minimal ("Please wait...").Build ());

                    var embed = new EmbedBuilder {
                        Color = new Color (0, 255, 0),
                        Author = new EmbedAuthorBuilder {
                        Name = uname,
                        IconUrl = $"http://a.ppy.sh/{u.Userid}",
                        Url = $"https://osu.ppy.sh/u/{u.Userid}"
                        },
                        Timestamp = DateTime.UtcNow,
                        Footer = new EmbedFooterBuilder {
                        Text = $"Command executed by {c.User.Username}",
                        IconUrl = c.User.GetAvatarUrl ()
                        }
                    };

                    var sb = new StringBuilder ();

                    foreach (var sc in scores) {
                        sb.Clear ();
                        var bm = await api.GetBeatmapAsync (sc.BeatmapId, BeatmapType.ByDifficulty, mode);
                        var bmstr = "";
                        if (sc.MaxCombo == null && bm.MaxCombo == null) {
                            bmstr = $"\u200B\t| **PP:** {sc.Pp,-20} **Score:** {sc.ScorePoints.ToFixed(),-20}\n";
                        } else if (bm.MaxCombo == null) {
                            bmstr = $"\u200B\t| **PP:** {sc.Pp,-20} **Score:** {sc.ScorePoints.ToFixed(),-20} **Combo:** {sc.MaxCombo?.ToFixed(),-20}\n";
                        } else if (sc.MaxCombo == null) {
                            bmstr = $"\u200B\t| **PP:** {sc.Pp,-20} **Score:** {sc.ScorePoints.ToFixed(),-20} **Combo:** {bm.MaxCombo?.ToFixed(),-20}\n";
                        } else {
                            bmstr = $"\u200B\t| **PP:** {sc.Pp,-20} **Score:** {sc.ScorePoints.ToFixed(),-20} **Combo:** {sc.MaxCombo?.ToFixed()} / {bm.MaxCombo?.ToFixed()}\n";
                        }
                        sb.AppendLine (bmstr +
                            $"\t| **CS:** {bm.CircleSize, -25} **AR:** {bm.ApproachRate, -20} **HP:** {bm.HpDrain, -20} **OD:** {bm.OverallDifficulty, -20}\n" +
                            $"\t| **Date:** {sc.Date, -20} | [Link](https://osu.ppy.sh/s/{bm.BeatmapsetId})\n");
                        if ((c.Guild.GetUser (c.Client.CurrentUser.Id)).Roles.Any (x => x.Permissions.UseExternalEmojis)) {
                            embed.AddField (x => {
                                x.Name = $"**{bm.Artist} - {bm.Title} [{bm.Difficulty}] by {bm.Creator}** | {bm.DifficultyRating.ToFixed()} ☆ {sc.Mods}";
                                x.Value = sb.ToString ();
                                x.IsInline = false;
                            });
                        } else {
                            embed.AddField (x => {
                                x.Name = $"**{bm.Artist} - {bm.Title} [{bm.Difficulty}] by {bm.Creator}** | {bm.DifficultyRating.ToFixed()} ☆ {sc.Mods}";
                                x.Value = sb.ToString ();
                                x.IsInline = false;
                            });
                        }
                    }
                    await mc.DeleteAsync ();
                    return embed;
                } catch (Exception ex) {
                    var embed = NeoEmbeds.Minimal (ex.Message).AddField ("inner", ex.InnerException?.Message ?? "nope");
                    await NeoConsole.NewLine (ex.StackTrace);
                    return embed;
                }
            }).Result;
        }

        public static EmbedBuilder GetUserRecent5ByNameAndModAsync (SocketCommandContext c, string uname, GameMode mode = GameMode.Standard) {
            return Task.Run (async () => {
                var u = await api.GetUserByNameAsync (uname, mode);
                var scores = await api.GetUserRecentByUsernameAsync (uname, mode, 5);

                if (u == null || scores == null || scores.Count <= 0) {
                    return NeoEmbeds.Minimal ($"Could not found recent scores by user `{uname}`");
                }

                var mc = await c.Channel.SendMessageAsync ("", false, NeoEmbeds.Minimal ("Please wait...").Build ());

                var embed = new EmbedBuilder {
                    Color = new Color (0, 255, 0),
                    Author = new EmbedAuthorBuilder {
                    Name = uname,
                    IconUrl = $"http://a.ppy.sh/{u.Userid}",
                    Url = $"https://osu.ppy.sh/u/{u.Userid}"
                    },
                    Timestamp = DateTime.UtcNow,
                    Footer = new EmbedFooterBuilder {
                    Text = $"Command executed by {c.User.Username}",
                    IconUrl = c.User.GetAvatarUrl ()
                    }
                };
                var sb = new StringBuilder ();

                foreach (var sc in scores) {
                    sb.Clear ();
                    var bm = await api.GetBeatmapAsync (sc.BeatmapId, BeatmapType.ByDifficulty, mode);
                    var bmstr = "";
                    if (sc.MaxCombo == null && bm.MaxCombo == null) {
                        bmstr = $"\u200B\t| **Score:** {sc.ScorePoints.ToFixed(),-20}\n";
                    } else if (bm.MaxCombo == null) {
                        bmstr = $"\u200B\t| **Score:** {sc.ScorePoints.ToFixed(),-20} **Combo:** {sc.MaxCombo?.ToFixed()}\n";
                    } else if (sc.MaxCombo == null) {
                        bmstr = $"\u200B\t| **Score:** {sc.ScorePoints.ToFixed(),-20} **Combo:** {bm.MaxCombo?.ToFixed()}\n";
                    } else {
                        bmstr = $"\u200B\t| **Score:** {sc.ScorePoints.ToFixed(),-20} **Combo:** {sc.MaxCombo?.ToFixed()} / {bm.MaxCombo?.ToFixed()}\n";
                    }
                    sb.AppendLine (bmstr +
                        $"\t| **Mods:** {sc.Mods} **Star:** {bm.DifficultyRating.ToFixed()} **CS:** {bm.CircleSize} **AR:** {bm.ApproachRate} **OD:** {bm.OverallDifficulty} **HP:** {bm.HpDrain}\n" +
                        $"\t| **Date:** {sc.Date} | [Link](https://osu.ppy.sh/s/{bm.BeatmapsetId})\n");

                    if ((c.Guild.GetUser (c.Client.CurrentUser.Id)).Roles.Any (x => x.Permissions.UseExternalEmojis)) {
                        embed.AddField (x => {
                            x.Name = $"**{bm.Artist} - {bm.Title} [{bm.Difficulty}] by {bm.Creator}** | {bm.DifficultyRating.ToFixed()} ☆ {ModReplacer(string.Join(",", ((Mods)sc.EnabledMods).GetArray().Where(m => m != Mods.None)))}";
                            x.Value = sb.ToString ();
                            x.IsInline = false;
                        });
                    } else {
                        embed.AddField (x => {
                            x.Name = $"**{bm.Artist} - {bm.Title} [{bm.Difficulty}] by {bm.Creator}** | {bm.DifficultyRating.ToFixed()} ☆ {string.Join(",", ((Mods)sc.EnabledMods).GetArray().Where(m => m != Mods.None))}";
                            x.Value = sb.ToString ();
                            x.IsInline = false;
                        });
                    }
                }
                await mc.DeleteAsync ();
                return embed;
            }).Result;
        }


        public static string Thing(int x)
        {
            var time = TimeSpan.FromSeconds(Convert.ToDouble(x));
            return time.ToString(@"hh\:mm\:ss");
        }
        public static EmbedBuilder GetBeatmapDetailsFromId (SocketCommandContext c, ulong beatmapid) {
            return Task.Run (async () => {
                
                var beatmap = await api.GetBeatmapAsync(beatmapid);

                var embed = new EmbedBuilder {
                    Color = new Color (0, 255, 0),
                    ThumbnailUrl = $"https://b.ppy.sh/thumb/{beatmap.BeatmapsetId}l.jpg",
                    Footer = new EmbedFooterBuilder {
                    Text = $"{(beatmap.Approved == BeatmapState.Approved ? $"Ranked at {beatmap.ApprovedDate}" : "Not Ranked")}"
                    }
                };

                embed.AddField(x => {
                    x.Name = $"{beatmap.Title} by {beatmap.Creator}";
                    x.Value = $"**Length:** {Thing(beatmap.TotalLength)} **BPM:** {beatmap.Bpm}\n**Download:** [map](https://osu.ppy.sh/d/{beatmap.BeatmapsetId})([no vid](https://osu.ppy.sh/d/{beatmap.BeatmapsetId}n))";
                });
                embed.AddField(x => {
                    x.Name = beatmap.Difficulty;
                    x.Value = $"**Difficulty:** {beatmap.DifficultyRating.ToFixed()} ☆ **Max Combo:** {beatmap.MaxCombo}x\n"
                    + $"**AR:** {beatmap.ApproachRate.ToFixed()} **OD:** {beatmap.OverallDifficulty.ToFixed()} **HP:** {beatmap.HpDrain.ToFixed()} **CS:** {beatmap.CircleSize.ToFixed()}\n";
                });
                return embed;
            }).Result;
        }
    }
}